function onLoad(onLoaded) {
  if (document.readyState == "interactive" || document.readyState == "complete")
    onLoaded();
  else window.addEventListener("DOMContentLoaded", onLoaded);
}

export default onLoad;
