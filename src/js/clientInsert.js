import Client from "./Client.js";

window.client = new Client({
  endpoint: `${window.location.origin}/api`,
  token: window.localStorage.getItem("token")
});

window.profilePromise = window.client
  .getProfile()
  .then(r => r)
  .catch(() => {});
